/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.wikipedia;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class BaseBot extends WMFWiki {

    /**
     * Creates a new wiki that has the given domain name.
     *
     * @param domain a wiki domain name e.g. en.wikipedia.org
     */
    public BaseBot(String domain) {
        super(domain);
        this.setUserAgent("BaseBot");
        this.setMaxLag(1000);
        this.setMarkBot(true);
        this.setMarkMinor(true);
    }

    /**
     * @param title - title of the page in the site
     * @param site - the site the page is in
     * @return
     * @throws IOException
     */
    public boolean hasWDEntity(String title, String site) throws IOException {
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = query + "action=wbgetentities&redirects=yes&props=sitelinks&sites=" + site + "&titles=" + URLEncoder.encode(title, "UTF-8");
        String line = fetch(url, "hasWDEntity");
        boolean r = false;
        if (!line.contains("missing=\"\"")) {
            r = true;
        }
        return r;
    }

    /**
     * Checks if a user has uploads between the specified times.
     *
     * @param user the user to get uploads for
     * @param start the date to start enumeration (use null to not specify one)
     * @param end the date to end enumeration (use null to not specify one)
     * @return a list of all live images the user has uploaded
     * @throws IOException if a network error occurs
     */
    public boolean hasUploads(Wiki.User user, Calendar start, Calendar end) throws IOException {
        StringBuilder url = new StringBuilder(query);
        url.append("list=allimages&ailimit=1&aisort=timestamp&aiprop=timestamp%7Ccomment&aiuser=");
        url.append(encode(user.getUsername(), false));
        if (start != null) {
            url.append("&aistart=");
            url.append(calendarToTimestamp(start));
        }
        if (end != null) {
            url.append("&aiend=");
            url.append(calendarToTimestamp(end));
        }
        List<Wiki.LogEntry> uploads = new ArrayList<>();

        String line = fetch(url.toString(), "getUploads");

        Boolean result = line.contains("<img ");

        log(Level.INFO, "getUploads", "Successfully retrieved uploads of\t" + user.getUsername() + "\t(\t" + result + "\t)");

        return result;
    }

    /**
     * @param id
     * @return
     * @throws IOException
     */
    public Map<String, String> getSiteLinks(String id) throws IOException {
        Map<String, String> data = new HashMap<>();
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = query + "action=wbgetentities&redirects=yes&props=sitelinks&ids=";
        url += URLEncoder.encode(id, "UTF-8");
        String line = fetch(url, "getSiteLinks");
        if (line.contains("<sitelinks>")) {
            line = line.substring(line.indexOf("<sitelinks>"), line.indexOf("</sitelinks>"));
            String[] links = line.split("<sitelink ");

            for (int i = 1; i < links.length; i++) {
                String link = links[i];
                String site = parseAttribute(link, "site", 0);
                String title = parseAttribute(link, "title", 0);
                data.put(site, title);
            }
        }
        return data;
    }

    /**
     * @param title - title of the page in the site
     * @param site - the site the page is in
     * @return
     * @throws IOException
     */
    public int WDEntity(String title, String site) throws IOException {
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = query + "action=wbgetentities&redirects=yes&props=sitelinks&sites=" + site + "&titles=" + URLEncoder.encode(title, "UTF-8");
        String line = fetch(url, "WDEntity");
        int r = 0;
        if (!line.contains("missing=\"\"")) {
            r = 0;
        }

        int a = line.indexOf(" id=\"Q") + 6;
        int b = line.indexOf("\"", a);
        r = Integer.parseInt(line.substring(a, b));

        return r;
    }

    /**
     * Serves for resolving redirects
     *
     * @param qid
     * @return
     * @throws IOException
     */
    public String[] WDEntity(String... qid) throws IOException {
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = query + "action=wbgetentities&redirects=yes&props=&ids=";
        String ids = "";
        for (int i = 0; i < qid.length; i++) {
            ids += "".equals(ids) ? qid[i] : "|" + qid[i];
        }
        url += ids;
        String line = fetch(url, "WDEntity");
        String r = "";
        if (line.contains("missing=\"\"")) {
            r = null;
        } else {

            boolean run = true;
            while (run) {
                int a = line.indexOf(" id=\"Q") + 6;
                int b = line.indexOf("\"", a);
                r += "".equals(r) ? line.substring(a, b) : "|" + line.substring(a, b);
                line = line.substring(b);
                run = line.contains(" id=\"Q");
            }
        }

        return r.split("\\|");
    }

    public String[] WDEntityIsHuman(String title, String site) throws IOException {
        //wbgetentities&format=xml&sites=ruwiki&titles=
        String url = /*query*/ "https://www.wikidata.org/w/api.php?format=xml&maxlag=5&action=query&" + "action=wbgetentities&redirects=yes&props=sitelinks|claims&sites=" + site + "&titles=" + URLEncoder.encode(title, "UTF-8");
        String line = fetch(url, "WDEntity");
        //System.out.println(line);
        String id;
        String humanity = "no";
        String female = "no";
        if (line.contains("missing=\"\"")) {
            return null;
        }

        int a = line.indexOf(" id=\"Q") + 6;
        int b = line.indexOf("\"", a);
        id = line.substring(a, b);

        if (line.contains("<property id=\"P31\">")) {
            int d = line.indexOf("<property id=\"P31\">");
            a = line.indexOf("<mainsnak", d);
            b = line.indexOf("</mainsnak>", a);
            String stack = line.substring(a, b);
            if (stack.contains("<value entity-type=\"item\" numeric-id=\"5\" />")) {
                humanity = "yes";
            }
        }

        if (line.contains("<property id=\"P21\">")) {
            int d = line.indexOf("<property id=\"P21\">");
            a = line.indexOf("<mainsnak", d);
            b = line.indexOf("</mainsnak>", a);
            String snack = line.substring(a, b);
            if (snack.contains("<value entity-type=\"item\" numeric-id=\"6581072\" />")) {
                female = "yes";
                System.out.println("FEMALE");

            } else {
                System.out.println("NOT A FEMALE IT SEEMS");
            }
        }

        String[] r = {id, humanity, female};

        return r;
    }

    ;
    

    public int CAId(String user) throws IOException {
        //action=query&meta=globaluserinfo&guiuser=Example&format=xml
        String url = query + "meta=globaluserinfo&guiuser=" + URLEncoder.encode(user, "UTF-8");
        String line = fetch(url, "CAId");
        int r = 0;

        try {
            r = Integer.parseInt(parseAttribute(line, "id", 0));
        } catch (java.lang.NumberFormatException ex) {
            System.out.println("Error on " + url + "\n" + line);
        }

        return r;
    }

    public Map<String, String> batchPageTexts(String batchurl) throws Exception {
        //https://uk.wikipedia.org/w/api.php?action=query&format=xml&prop=revisions&list=&meta=&titles=Почтальон+Печкин|Ата&redirects=1&rvprop=content 
        String url = query + "format=xml&prop=revisions&list=&meta=&rvprop=content"/*+"&titles=" + URLEncoder.encode(batchurl, "UTF-8")*/;
//        String line = fetch(url, "batchPageTexts");

        StringBuilder buffer = new StringBuilder(300000);
        buffer.append("titles=");
        buffer.append(URLEncoder.encode(batchurl, "UTF-8"));

        String line = post(url, buffer.toString(), "batchPageTexts");
        Map<String, String> returnie = new HashMap<>();
        int i = 0;
        boolean cont = false;
        cont = line.contains("<page ");
        while (cont) {
            //System.out.println(i++);
            String pageendtag = "</page>";
            int indexOfPageEnd = line.indexOf(pageendtag);
            if (indexOfPageEnd == -1) {
                break;
            }
            //System.out.println(indexOfPageEnd);

            String page = line.substring(0, indexOfPageEnd);
            line = line.substring(indexOfPageEnd + pageendtag.length());
            cont = line.contains("<page ");
            if (!page.contains("</rev>")) {
                continue;
            }
            String pagemetadata = page.replaceAll("(?s).*(<page[^>]*?>).*", "$1");
            //System.out.println(pagemetadata);
            String pagetitle = pagemetadata.replaceAll("(?s).*title=\"(.*?)\".*", "$1").trim();
            String pagetext = decode(page.replaceAll("(?s).*<rev[^>]*?>\n?(.*)\n?</rev>.*", "$1").trim());
            if ("".equals(pagetitle) || "".equals(pagetext)) {
                continue;
            }
            returnie.put(pagetitle, pagetext);
        }

        return returnie;

    }

    //my personal code тут розпочинається те що я в напівбреді накодив
   
    /*
     * повертає назву випадкового редиректу
     * 
     */
    public String randomRedirect(int namespace) throws IOException {
        // fetch
        String url = query + "action=query&list=random";
        url += (namespace == ALL_NAMESPACES ? "" : "&rnnamespace=" + namespace);
        url += "&rnredirect=";
        String line = fetch(url, "random");

        // parse
        int a = line.indexOf("title=\"") + 7;
        int b = line.indexOf('\"', a);
        return line.substring(a, b);
    }

    public String[] allPages(String title, int namespace, boolean redirects) throws IOException {
        StringBuilder url = new StringBuilder(query);
        url.append("action=query&list=allpages&aplimit=max&apfrom=");
        url.append(URLEncoder.encode(title, "UTF-8"));
        if (namespace != ALL_NAMESPACES) {
            url.append("&apnamespace=");
            url.append(namespace);
        }
        if (redirects) {
            url.append("&apfilterredir=redirects");
        }

        // main loop
        ArrayList<String> pages = new ArrayList<String>(6667); // generally enough
        String temp = url.toString();
        String next = "";
        do {
            // fetch data
            String line = fetch(temp + next, "allPages");

            // set next starting point
            if (line.contains("apcontinue")) {
                int a = line.indexOf("apcontinue=\"") + 12;
                int b = line.indexOf('\"', a);
                next = "&apcontinue=" + line.substring(a, b);
            } else {
                next = "done";
            }

            // parse items
            while (line.contains("title")) {
                int x = line.indexOf("title=\"");
                int y = line.indexOf("\" ", x);
                pages.add(decode(line.substring(x + 7, y)));
                line = line.substring(y + 4);
            }
        } while (!next.equals("done"));

        log(Level.INFO, "Successfully retrieved " + (redirects ? "list of pages " : "list of redirects") + " (" + pages.size() + " items)", "allPages");
        return pages.toArray(new String[0]);
    }


    public String[] yandexGeocode(String loc) throws IOException {
        String url = "http://geocode-maps.yandex.ru/1.x/?lang=uk-UA&results=1&geocode=" + URLEncoder.encode(loc, "UTF-8");
        String line = fetch(url, "getImageHistory");
        int a = line.indexOf("<pos>") + 5;
        int b = line.indexOf("</pos>", a);
        String coords = line.substring(a, b);
        System.out.println(loc + "    : " + coords);
        return coords.split("\\s");
    }

    /**
     *
     * @param ucuser - Ім'я користувача
     * @param ucstart - Як не дивно кінцева дата (часова мітка) наприклад для
     * охоплення березня це 2013-03-31T23:59:59Z
     * @param ucend - як не дивно початкова дата (часова мітка) наприклад для
     * охоплення березня це 2013-03-01T00:00:00Z
     * @return Сума різниць розмірів усіх редагувань користувача за проміжок
     * часу, в нс 0
     * @throws Exception
     */
    public int[] contribsDiffSummator(String ucuser, String ucstart, String ucend) throws Exception {
        // prepare the url
        StringBuilder urlBuild = new StringBuilder(query);
        urlBuild.append("action=query&list=usercontribs&ucnamespace=0&uclimit=max");
        urlBuild.append("&ucprop=title%7Ctimestamp%7Csize%7Csizediff");
        urlBuild.append("&ucuser=").append(URLEncoder.encode(ucuser, "UTF-8"));
        urlBuild.append("&ucend=").append(ucend);
        urlBuild.append("&ucstart=");
        String url = urlBuild.toString();
        //System.out.println(url);
        String start = ucstart;
        int sizediffi = 0;
        boolean doing = true;
        Set<String> alreadyPresentHashes = new HashSet<>();
        do {
            String lines = fetch(url + start, "Wiki.contribsDiffSummator()");
            if (lines.contains("<usercontribs />")) {
                int[] res = {-1, 0};
                return res;// Якщо редагувань в проміжку немає, то нема чого парсити далі :)
            }
            String tag_opening = "<usercontribs>";
            int a = lines.indexOf(tag_opening) + tag_opening.length();
            int b = lines.indexOf("</usercontribs>", a);
            String itemss = lines.substring(a, b);
            String[] items = itemss.split("<item ");
            //System.out.println(items.length);
            for (int i = 1; i < items.length; i++) {
                String item = items[i];
                int aa = item.indexOf("sizediff=\"") + 10;
                int bb = item.indexOf("\"", aa);
                String sizediff = item.substring(aa, bb);
                int szdff = Integer.parseInt(sizediff);
                sizediffi += szdff;
            }
            if (lines.contains("<usercontribs ucstart=\"")) {
                System.out.println("Внесок " + ucuser + " більше ніж сторінка, беремо наступну сторінку");
                int aaa = lines.indexOf("<usercontribs ucstart=\"") + 23;
                int bbb = lines.indexOf("\"", aaa);
                start = lines.substring(aaa, bbb);
            } else {
                doing = false;
            }
        } while (doing);
        int[] res = {0, sizediffi};
        return res;

    }

    /**
     * <b>User must have contributions</b>
     *
     * @param ucuser
     * @return timestamp of the first edit
     * @throws Exception
     */
    public String firstEdit(String ucuser) throws Exception {
        StringBuilder temp = new StringBuilder(query);
        temp.append("action=query&list=usercontribs&ucnamespace=0&uclimit=1&ucdir=newer&ucnamespace=0&ucprop=timestamp");
        temp.append("&ucuser=");
        temp.append(URLEncoder.encode(ucuser, "UTF-8"));
        String url = temp.toString();
        //System.out.println(url);
        String lines = fetch(url, "Wiki.contribsDiffSummator()");
        int a = lines.indexOf("timestamp=\"") + 11;
        int b = lines.indexOf("\"", a);
        String res = lines.substring(a, b);
        return res;
    }
    //&list=usercontribs&format=xml&uclimit=1&ucuser=Base&ucdir=newer&ucnamespace=0&ucprop=timestamp

    public int[] wikinewsArticleCounter(String ucuser, String ucstart, String ucend) throws Exception {
        // prepare the url
        StringBuilder temp = new StringBuilder(query);
        temp.append("action=query&list=usercontribs&ucnamespace=0&uclimit=max");
        temp.append("&ucprop=title%7Ctimestamp%7Csize%7Csizediff%7Cflags");
        temp.append("&ucuser=" + URLEncoder.encode(ucuser, "UTF-8"));
        temp.append("&ucend=" + ucend);
        temp.append("&ucstart=");

        String url = temp.toString();
        //System.out.println(url);
        String start = ucstart;
        String allp = "";
        int just = 0;
        int original = 0;
        boolean doing = true;
        do {

            String lines = fetch(url + start, "Wiki.contribsDiffSummator()");
            //System.out.println(lines);
            if (lines.contains("<usercontribs />")) {
                int[] ret = {0, 0};
                return ret;// Якщо редагувань в проміжку немає, то нема чого парсити далі :)
            }

            int a = lines.indexOf("<usercontribs>") + 14;
            int b = lines.indexOf("</usercontribs>", a);
            String itemss = lines.substring(a, b);
            String[] items = itemss.split("<item ");
            //System.out.println(ucuser + "   " + items.length);
            for (int i = 1; i < items.length; i++) {
                String item = items[i];

                if (item.contains("new=\"\"")) {

                    //System.out.println(ucuser + "   " + item);
                    int aa = item.indexOf("title=\"") + 7;
                    int bb = item.indexOf("\"", aa);
                    String arti = decode(item.substring(aa, bb));

                    if (!isRedirect(arti)) {
                        String[] categories = getCategories(arti);
                        boolean orart = false;
                        for (int k = 0; k < categories.length; k++) {

                            if ("Авторський репортаж".equals(categories[k])) {
                                orart = true;
                                //System.out.println(ucuser + "   " + original);
                            } else {
                                //System.out.println(ucuser + "   " + just);
                            }

                        }

                        if (orart) {
                            original++;
                        } else {
                            just++;
                        }
                    } else {
                    }

                }

            }

            if (lines.contains("<usercontribs ucstart=\"")) {

                System.out.println("Внесок " + ucuser + " більше ніж сторінка, беремо наступну сторінку");

                int aaa = lines.indexOf("<usercontribs ucstart=\"") + 23;
                int bbb = lines.indexOf("\"", aaa);
                start = lines.substring(aaa, bbb);
            } else {
                doing = false;
            }

        } while (doing);
        int[] ret = {just, original};
        return ret;

    }

    public Boolean isRedirect(String title) throws Exception {
        String answer = fetch(query + "action=query&prop=info&titles=" + URLEncoder.encode(title, "UTF-8"), "BaseBot.isRedirect");
        Boolean ans = answer.contains("redirect=\"\"");
        //System.out.println("##########################################\n########################################\n"+title+ans);
        return ans;
    }
    
    public String getWikiId(){
        return this.wikiid;
    }
    
    
    
}
