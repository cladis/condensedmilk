package condensedmilk;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wikipedia.BaseBot;
import org.wikipedia.OldWiki;

/**
 * @author Base
 */
public class CondensedMilk {
    /**
     *
     * @param args логін, пароль, проект [ВП, ВЦ, ВН(б), ВБ, ВЖ], рік, місяць
     * або <tt>--help</tt>
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        if (args[0].matches("--help")) {
            showHelp();
            System.exit(4357);
        }
        //проект, рік, місяць
        initialiseVariables(Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]));
        writeVariable = "\n== Не нуби ==\n" + writeVariable;
        writeNoobsVariable = "\n== Нуби ==\n" + writeNoobsVariable;

        BaseBot w = new BaseBot("uk." + workProjectName + ".org");//w for wiki(pedia)
        w.login(args[0], args[1].toCharArray());
        w.getSiteInfo();
        System.out.println("w's domain:\t" + w.getDomain());

        BaseBot wm = new BaseBot("ua.wikimedia.org");//wm for wikimedia
        wm.login(args[0], args[1].toCharArray());

        System.out.println("w's domain:\t" + w.getDomain());

        System.out.println("Hey there!");
        final String[] allUsers = w.allUsers("", -1);
        System.out.println(allUsers);
        ExecutorService service = Executors.newCachedThreadPool();

        final BaseBot ww = w;
        final BaseBot wmwm = wm;
        for (int j = 0; j < numberOfThreads; j++) {
            final int n = j;
            service.submit(new Runnable() {
                public void run() {
                    try {
                        doing(n, ww, wmwm, allUsers);
                    } catch (Exception ex) {
                        Logger.getLogger(CondensedMilk.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        }
    }

    public static final void doing(int num, BaseBot w, BaseBot wm, String[] allUsers) throws Exception {
        //System.out.println(wm.getDomain());
        System.out.println(allUsers.length);
        boolean last = false;
        for (int i = 0 + num; i < allUsers.length; i += numberOfThreads) {
            String user = allUsers[i];
            int[] contribsDiffSummator = null;
            try {
                contribsDiffSummator = w.contribsDiffSummator(user, endingTimestamp, beginningTimestamp);
                if (Thread.interrupted()) {
                    throw new java.lang.InterruptedException("No idea why, "
                            + "but as of after collecting the contribs summ for "
                            + user
                            + " the thread is already interrupted");
                }
            } catch (Exception ex) {
                System.out.println(ex);
                System.exit(666);
            }
            if (contribsDiffSummator[0] > -1 && contribsDiffSummator[1] >= 0/*2000*/) {
                System.out.println(i + "    " + user + "   " + contribsDiffSummator[1]);
                String firstEdit = w.firstEdit(user);
                String[] firstEditParts = firstEdit.split("-");
                int firstEditYear = Integer.parseInt(firstEditParts[0]);
                int firstEditMonth = Integer.parseInt(firstEditParts[1]);
                System.out.println(user + "  year=" + firstEditYear + "  month" + firstEditMonth);
                boolean isNoob = (firstEditYear >= noobYear) &&
                        ( noobMonth+2 > 12 
                        ? (firstEditMonth >= noobMonth || (firstEditYear >= noobYear +1 && firstEditMonth > 0) ) 
                        : (firstEditMonth >= noobMonth) );
                String userpageLink = "[[" + workProjectName + ":uk:user:" + user + "|" + user + "]]";
                String contribsLink = "([//uk."
                        + workProjectName
                        + ".org/w/index.php?limit=5000&title=Special:Contributions&contribs=user&target="
                        + user.replaceAll("\\s", "_")
                        + "&year=" + workYear
                        + "&month=" + workMonth
                        + " внесок])";
                String userLine = "\n|-\n|" + userpageLink + " " + contribsLink + "||" + contribsDiffSummator[1] + "||" + firstEdit;
                if (!isNoob) {
                    writeVariable += userLine;
                } else {
                    writeNoobsVariable += userLine;
                }
            }

            if ((i + numberOfThreads) >= allUsers.length) {
                count++;
                System.out.println(count);
            }

            if (count == numberOfThreads) {
                last = true;
            }
        }

        if (last) {
            Thread.sleep(60000);
            writeVariable += "\n|}";
            writeNoobsVariable += "\n|}";
            System.out.println(writeNoobsVariable);
            System.out.println(writeVariable);
            String finalOutput = workProjectUkName + ". " + workMonthOutputName + "\n{{Згущівка поштою (bb) шапка}}\n" + writeNoobsVariable + writeVariable;
            wm.edit("Згущівка_поштою/bb:" + workProjectUkName + ((workYear >= 2016) ? ("/" + workYear + "/" + workMonth) : ""),
                    finalOutput,
                    workMonthOutputName + " — створення/оновлення/доповнення");
        }
    }

    static int workProject;
    static int numberOfThreads;
    static int minimumSum;
    static String[] workProjectNames;
    static String workProjectName;
    static int count = 0;
    static int workYear;
    static int workMonth;
    static String monthName;
    static int noobYear;
    static int noobMonth;
    static int daysInMonth;
    static String[] workProjectUkNames;
    static String workProjectUkName;
    static String workMonthOutputName;
    static String writeVariable;
    static String writeNoobsVariable;
    static String endingTimestamp;
    static String beginningTimestamp;

    public static void initialiseVariables(int the_project, int the_year, int the_month) {
        workProject = the_project;//число яке визначає в якому проекті працюватимемо відповідно до масивів
        workYear = the_year;//рік місяця, для якого рахуємо статистику
        workMonth = the_month;//номер місяця, у якому визначаємо статистику

        numberOfThreads = (workProject < 3) ? ((workProject == 0) ? 20 : 10) : 5; //число потоків, відповідно до того де в мене прапор бота
        minimumSum = (workProject == 0) ? 5000 : 0; //мінімальне число позитивної різниці байт щоб користувача було виведено

        String[][] workProjects = {
            {"wikipedia", "wikiquote", "wikinews", "wikibooks", "wikivoyage"},
            {"Вікіпедія", "Вікіцитати", "Вікіновини (байти)", "Вікіпідручник", "Віківояж"}
        };

        String[] workProjectsInit = workProjects[0];
        workProjectNames = workProjectsInit;
        workProjectName = workProjectNames[workProject]; //проект у якому вестиметься робота
        noobYear = workYear; //нубрік
        if (workMonth - 2 > 0) {
            noobMonth = workMonth - 2; //нубмісяць
        } else {
            noobMonth = workMonth - 2 + 12;
            noobYear = workYear - 1;
        }

        switch (workMonth) {
            case 1:
                monthName = "Січень";
                daysInMonth = 31;
                break;
            case 2:
                monthName = "Лютий";
                daysInMonth = (workYear % 4 == 0) ? 29 : 28;//by the time we need to consider the second condition, we are dead :)
                break;
            case 3:
                monthName = "Березень";
                daysInMonth = 31;
                break;
            case 4:
                monthName = "Квітень";
                daysInMonth = 30;
                break;
            case 5:
                monthName = "Травень";
                daysInMonth = 31;
                break;
            case 6:
                monthName = "Червень";
                daysInMonth = 30;
                break;
            case 7:
                monthName = "Липень";
                daysInMonth = 31;
                break;
            case 8:
                monthName = "Серпень";
                daysInMonth = 31;
                break;
            case 9:
                monthName = "Вересень";
                daysInMonth = 30;
                break;
            case 10:
                monthName = "Жовтень";
                daysInMonth = 31;
                break;
            case 11:
                monthName = "Листопад";
                daysInMonth = 30;
                break;
            case 12:
                monthName = "Грудень";
                daysInMonth = 31;
                break;
            default:
                throw new AssertionError();
        }

        String[] workProjectUkNamesInit = workProjects[1];
        workProjectUkNames = workProjectUkNamesInit;
        workProjectUkName = workProjectUkNames[workProject]; //заголовок сторінки, назва підсторінки
        workMonthOutputName = monthName + " " + workYear; //місяць у заголовку сторінки
        writeVariable = "{| class='wikitable sortable plainlinks'\n!Юзер!!Байти!!Перша правка";
        writeNoobsVariable = writeVariable;

        endingTimestamp = workYear + "-" + (workMonth < 10 ? "0" + workMonth : workMonth) + "-" + daysInMonth + "T23:59:59Z";
        beginningTimestamp = workYear + "-" + (workMonth < 10 ? "0" + workMonth : workMonth) + "-" + "01" + "T00:00:00Z";

        System.out.println("Бота запущено для підрахунку статистики за " + workMonthOutputName + " року для проекту " + workProjectUkName + " (елемент №" + workProject + " масиву проектів)");
        System.out.println("У робочому місяці днів: " + daysInMonth);
        System.out.println("Нубомісяцем вважається " + (noobMonth < 10 ? "0" + noobMonth : noobMonth) + "-" + noobYear);
        System.out.println("Обране число потоків: " + numberOfThreads);
        System.out.println("Межа суми різниць: " + minimumSum + "б");
    }

    public static void showHelp() {
        System.out.println("Бот підрахунку статистики для проекту «Згущівка поштою»");
        System.out.println("Параметри:");
        System.out.println("Логін");
        System.out.println("Пароль");
        System.out.println("Проект(число від 0 до 4, відповідає ВП, ВЦ, ВН (байти), ВБ, ВЖ)");
        System.out.println("Рік");
        System.out.println("Місяць (від 1 до 12)");
    }
}
//BUILD SUCCESSFUL (total time: 193 minutes 34 seconds)
