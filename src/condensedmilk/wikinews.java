package condensedmilk;

import java.io.InputStream;
import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wikipedia.OldWiki;

/**
 *
 * @author Base
 */
public class wikinews {

    /**
     * @param args the command line arguments
     */
    static int potoky = 10;
    static int counter = 0;
    static String pr = "wikinews";
//    static int noobYear = 2013;
//    static int noobMonth = 4;
    static String wrt = "{| class='wikitable sortable'\n!Юзер!!Число новин";
    static String awrt = wrt;

    public static void main(String[] args) throws Exception {

        wrt = "\n== Прості новини ==\n" + wrt;
        awrt = "\n== Оригінальні репортажі ==\n" + awrt;
        OldWiki w = new OldWiki("uk." + pr + ".org");//укрвікіцитати
        w.setUserAgent("WPBot 1.0");
        w.login(args[0], args[1].toCharArray());//вхід в ВЦ
        w.setMarkBot(true);
        w.setMarkMinor(true);

        OldWiki wm = new OldWiki("ua.wikimedia.org");//укрвікіцитати
        wm.setUserAgent("WPBot 1.0");
        wm.login(args[0], args[1].toCharArray());//вхід в ВЦ
        wm.setMarkBot(true);
        wm.setMarkMinor(true);
        final String login = args[0];
        final String pass = args[1];
        final String[] au = w.allUsers("");
        ExecutorService service = Executors.newCachedThreadPool();
        for (int j = 0; j < potoky; j++) {
            final int n = j;
            service.submit(new Runnable() {
                public void run() {
                    try {
                        doing(n, login, pass, au);
                    } catch (Exception ex) {
                        Logger.getLogger(CondensedMilk.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        }

    }

    public static final void doing(int num, String login, String pass, String[] au) throws Exception {
        OldWiki w = new OldWiki("uk." + pr + ".org");//укрвікіцитати
        w.setUserAgent("WPBot 1.0");
        w.login(login, pass.toCharArray());//вхід в ВЦ
        w.setMarkBot(true);
        w.setMarkMinor(true);



        System.out.println(au.length);
        boolean last = false;
        for (int i = 0 + num; i < au.length; i += potoky) {
            String u = au[i];
            //System.out.println(i + "    " + u);
            int[] wikinewsArticleCounter = w.wikinewsArticleCounter(u, "2013-11-30T23:59:59Z", "2013-11-01T00:00:00Z");
            if (wikinewsArticleCounter[0] > 0 || wikinewsArticleCounter[1] > 0) {

                System.out.println(i + "    " + u + "   " + wikinewsArticleCounter[0] +" | "+ wikinewsArticleCounter[1]);
                int autors = wikinewsArticleCounter[1];
                int jusarts = wikinewsArticleCounter[0];


                if (jusarts > 0) {
                    wrt += "\n|-\n|[[" + pr + ":uk:user:" + u + "|" + u + "]] ([[:" + pr + ":uk:Special:Contributions/" + u + "|внесок]])||" + jusarts;
                }
                if (autors > 0) {
                    awrt += "\n|-\n|[[" + pr + ":uk:user:" + u + "|" + u + "]] ([[:" + pr + ":uk:Special:Contributions/" + u + "|внесок]])||" + autors;
                }
            }

            if (i + potoky >= au.length) {
                counter++;
            }

            if (counter == potoky) {
                last = true;
            }
        }



        if (last) {
            //Thread.sleep(60000);
            OldWiki wm = new OldWiki("ua.wikimedia.org");//укрвікіцитати
            wm.setUserAgent("WPBot 1.0");
            wm.login(login, pass.toCharArray());//вхід в ВЦ
            wm.setMarkBot(true);
            wm.setMarkMinor(true);

            wrt += "\n|}";

            awrt += "\n|}";
            System.out.println(wrt);


            System.out.println(awrt);
            String ttle = "Вікіновини";
            String mo = "Листопад 2013";
            String fullout = ttle + ". " + mo + "\n{{Згущівка поштою (bb) шапка}}\n" + wrt + awrt;

            wm.edit("Згущівка_поштою/bb:" + ttle, fullout, mo + " — створення/оновлення/доповнення");
        }
    }
}
//BUILD SUCCESSFUL (total time: 193 minutes 34 seconds)